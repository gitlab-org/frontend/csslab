# Checklist
- [ ] Add CSS styles
- [ ] Document component on [design.gitlab.com](https://gitlab.com/gitlab-org/design.gitlab.com)
- [ ] Increment csslab version and publish to npm
- [ ] Update csslab version on gitlab-ce and update all components affected

# Specification

INSERT_SPECIFICATION_LINK_HERE

/label ~"CSS Component" ~frontend
