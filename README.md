# CSSLab
[![npm version](https://badge.fury.io/js/%40gitlab-org%2Fcsslab.svg)](https://badge.fury.io/js/%40gitlab-org%2Fcsslab)

* Will produce a minifed and full versions. Each containing a version with and without Bootstrap. 
```
yarn css
```

* See `package.json` for more build options.

## Lint
```
yarn lint
```

## How to publish

Note: Only [members](https://www.npmjs.com/org/gitlab-org/members) of npm group `@gitlab-org` are able to publish this project.

1. Update the package version number
2. `npm publish --access public`
