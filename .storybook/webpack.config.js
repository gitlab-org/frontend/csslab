module.exports = (baseConfig, env, defaultConfig) => {
  defaultConfig.module.rules.push({
    test: /\.html$/,
    exclude: /node_modules/,
    loader: 'html-loader'
  });

  return defaultConfig;
};
