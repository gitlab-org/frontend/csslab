import { storiesOf } from '@storybook/vue';
import headers from './headers';
import lists from './lists';
import misc from './misc';
import blockCodeSnippet from './block-code-snippet/';

storiesOf('Markdown type', module)
  .add('h1', headers.h1)
  .add('h2', headers.h2)
  .add('h3', headers.h3)
  .add('h4', headers.h4)
  .add('h5', headers.h5)
  .add('h6', headers.h6)
  .add('anchor link', misc.anchor)
  .add('anchor link (monospace)', misc.anchorMonospace)
  .add('mention', misc.mention)
  .add('code snippet', misc.codeSnippet)
  .add('code snippet link', misc.codeSnippetLink)
  .add('block code snippet (white)', blockCodeSnippet.white)
  .add('block code snippet (dark)', blockCodeSnippet.dark)
  .add('block code snippet (monokai)', blockCodeSnippet.monokai)
  .add('block code snippet (solarized light)', blockCodeSnippet.solarizedLight)
  .add('block code snippet (solarized dark)', blockCodeSnippet.solarizedDark)
  .add('color chip', misc.colorChip)
  .add('blockquote', misc.blockquote)
  .add('addition/deletions', misc.additionDeletion)
  .add('unordered list', lists.unordered)
  .add('ordered list', lists.ordered)
  .add('task lists', lists.task)
  .add('rtl lists', lists.rtl)
  .add('emoji', misc.emoji)
  .add('table', misc.table);
