const path = require('path');
const sassTrue = require('sass-true');

// Enables 'scss' import alias within test files
function importer(url) {
  if (url[0] === 'scss') {
    url = path.resolve(__dirname, '../scss/', url.substr(1));
  }

  return { file: url };
}

// Test Files
const functionsSuite = path.join(__dirname, '_bundle.scss');

// Run Tests
sassTrue.runSass({ importer, file: functionsSuite}, describe, it);
